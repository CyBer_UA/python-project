#!/usr/bin/python
class ProtocolEnum():
    AUTHENTICATION = "--authentication--: "
    REGISTRATION = "--registration--: "
    SESSION = "--SESSION--: "
    REQUEST_END = "\0"
    DATA = "--DATA--: "
    LOGIN = "--LOGIN--: "
    COMMAND = "--COMMAND--: "
    STATUS = "--STATUS--: "

#!/usr/bin/python
class RequestTypeEnum():
    AUTHENTICATION = "authentication"
    REGISTRATION = "registration"
    TRANSLATE = "translate"
    HISTORY = "history"
    RESULT = "result"

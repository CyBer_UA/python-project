#!/usr/bin/python
import random
import re

from RequestTypeEnum import RequestTypeEnum

SPLIT_SYMBOL = ":::"


def getUserDB():
    res = {}
    dbFile = open("user.db", "r+")
    dictionary = dbFile.read().strip()
    if (len(dictionary) == 0):
        return {}

    for x in dictionary.split("\r\n"):
        data = re.split(SPLIT_SYMBOL, x)
        res[data[0]] = {
            "password": data[1],
            "sessionList": []
        }

    dbFile.close()
    return res


userDB = getUserDB()


def isAuthFrame(frame):
    return frame["command"] == RequestTypeEnum.AUTHENTICATION or frame["command"] == RequestTypeEnum.REGISTRATION


def isUserValidSession(user, sessionId):
    return sessionId in userDB[user]["sessionList"]


def action(request):
    data = request["data"]
    login = data["login"]
    password = data["password"]
    if request["command"] == RequestTypeEnum.AUTHENTICATION:
        if login not in userDB:
            raise Exception("Login %s not exists" % login)

        user = userDB[login]
        if user and (user["password"] == password):
            session = str(random.getrandbits(64))
            user["sessionList"].append(session)
            return session

        raise Exception('Can not login')

    elif request["command"] == RequestTypeEnum.REGISTRATION:
        if login in userDB:
            raise Exception("User with login %s exists" % login)

        userDB[login] = {"password": password, "sessionList": []}
        session = str(random.getrandbits(64))
        userDB[login]["sessionList"].append(session)
        userDBFile = open("user.db", "a")
        userDBFile.write(login + SPLIT_SYMBOL + password + "\r\n")
        userDBFile.close()
        return session

#!/usr/bin/env python
import socket

from RequestTypeEnum import RequestTypeEnum
from protocolParser import buildDataFrame, parseProtocolFrame, buildSessionFrame

HOST = '127.0.0.1'
PORT = 6666
server_address = (HOST, PORT)
languageFrom = None
languageTo = None
login = None
sesionId = None

while 1:

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(server_address)
    try:
        if ((login is None) or (sesionId is None)):
            value = raw_input("write 1 for login and 2 for signup: ")
            login = raw_input("login: ")
            password = raw_input("password: ")
            frame = None
            if (value == "1"):
                frame = buildDataFrame(RequestTypeEnum.AUTHENTICATION, {"login": login, "password": password})

            if (value == "2"):
                frame = frame = buildDataFrame(RequestTypeEnum.REGISTRATION, {"login": login, "password": password})

            sock.send(frame)
            protocol_frame = parseProtocolFrame(sock)
            if protocol_frame["status"] != "200":
                print protocol_frame["data"]["error"]
                login = None
                password = None
                continue

            sesionId = protocol_frame["session"]

        if ((languageFrom is None) or (languageTo is None)):
            languageFrom = raw_input("Write language from (use language code: en, pl, ru, de ...): ")
            languageTo = raw_input("Write language to (use language code: en, pl, ru, de ...): ")
        else:
            print (
            "(" + languageFrom + "->" + languageTo + ") for change language write ':c' or ':h' for histoty or :q for exit \n")

        text = raw_input("Write transltation text: ")
        if ":c" in text:
            languageFrom = None
            languageTo = None
            continue
        elif ":q" in text:
            sock.close()
            break

        elif ":h" in text:
            req = buildSessionFrame(RequestTypeEnum.HISTORY, {"login": login}, sesionId)
            sock.send(req)
            protocol_frame = parseProtocolFrame(sock)

            if protocol_frame["status"] != "200":
                print protocol_frame["data"]["error"]
                continue

            answer = protocol_frame["data"]["text"]
            print ("\nyour histoty: \n" + answer + "\n")
        else:
            req = buildSessionFrame(RequestTypeEnum.TRANSLATE, {"from": languageFrom, "to": languageTo, "login": login,
                                                                "text": unicode(text, "utf-8")}, sesionId)
            sock.send(req)
            protocol_frame = parseProtocolFrame(sock)

            if protocol_frame["status"] != "200":
                print protocol_frame["data"]["error"]
                continue

            answer = protocol_frame["data"]["text"]

            print ("\nyour transltation: " + answer + "\n")

    except socket.error, e:
        print (e)

sock.close()

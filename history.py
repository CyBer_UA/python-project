#!/usr/bin/python
from RequestTypeEnum import RequestTypeEnum


def isHistoryFrame(frame):
    return frame["command"] == RequestTypeEnum.HISTORY

#!/usr/bin/python
# -*- coding: utf-8 -*-

LINE_END = "\r\n"
SPLIT_SYMBOL = "###"


def addToLog(frame, translation):
    logFile = open("log.txt", "a")
    data = frame["data"]
    if isinstance(translation, str):
        translation = unicode(translation, "utf-8")

    log = [data["login"], data["from"], data["text"], data["to"], translation]
    logFile.write(SPLIT_SYMBOL.join(log).encode("utf-8") + LINE_END)
    logFile.close()


def readFromLog(data):
    login = data["login"]
    res = ""
    logFile = open("log.txt", "r+")
    lines = logFile.read().strip().split(LINE_END)
    logFile.close()

    for x in lines:
        data = x.split(SPLIT_SYMBOL)
        if (data[0] != login):
            continue

        res += data[2] + " (" + data[1] + " -> " + data[3] + ") " + data[4] + "\n"

    return res

#!/usr/bin/python

from RequestTypeEnum import RequestTypeEnum
from auth import isUserValidSession, action, isAuthFrame
from history import isHistoryFrame
from logger import addToLog, readFromLog
from protocolParser import parseProtocolFrame, buildDataFrame, buildSessionFrame
from translate import translate, isTranslateFrame


def router(connection):
    frame = parseProtocolFrame(connection)

    try:
        if isAuthFrame(frame):
            sessionId = action(frame)
            return buildSessionFrame(RequestTypeEnum.AUTHENTICATION, {}, sessionId)

        if isTranslateFrame(frame) and isUserValidSession(frame["data"]["login"], frame["session"]):
            translation = translate(frame["data"])
            addToLog(frame, translation)
            res = buildDataFrame(RequestTypeEnum.RESULT, {"text": translation})
            return res

        elif isHistoryFrame(frame) and isUserValidSession(frame["data"]["login"], frame["session"]):
            return buildDataFrame(RequestTypeEnum.RESULT, {"text": readFromLog(frame["data"])})

        raise Exception("unknown command")

    except Exception, e:
        return buildDataFrame(RequestTypeEnum.RESULT, {"error": str(e)}, 400)

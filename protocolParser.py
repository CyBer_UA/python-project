#!/usr/bin/python
import json

from ProtocolEnum import ProtocolEnum

LINE_END = "\r\n"


# COMMAND: TRANSLATE
# DATA:EN-PL\r\n
# session
# \0

# COMMAND: auth
# LOGIN:login\r\n
# password: xxxx\r\n
# \0

# COMMAND: RESULT
# STATUS: 200
# data: login\r\n
# \0

def getProtocolFrame(connection):
    data = ""
    while not data.endswith(ProtocolEnum.REQUEST_END):
        data += connection.recv(1)
    return data


def buildDataFrame(command, data, status=200):
    res = ProtocolEnum.COMMAND + command + LINE_END
    res += ProtocolEnum.DATA + json.dumps(data) + LINE_END
    res += ProtocolEnum.STATUS + str(status) + LINE_END
    return res + ProtocolEnum.REQUEST_END


def buildSessionFrame(command, data, sessionId, status=200):
    res = ProtocolEnum.COMMAND + command + LINE_END
    res += ProtocolEnum.DATA + json.dumps(data) + LINE_END
    res += ProtocolEnum.SESSION + sessionId + LINE_END
    res += ProtocolEnum.STATUS + str(status) + LINE_END
    return res + ProtocolEnum.REQUEST_END


def parseProtocolFrame(connection):
    res = {}
    data = getProtocolFrame(connection).split(LINE_END)

    for line in data:

        if line.startswith(ProtocolEnum.REQUEST_END):
            break
        elif line.startswith(ProtocolEnum.COMMAND):
            res["command"] = line.split(ProtocolEnum.COMMAND)[1]

        elif line.startswith(ProtocolEnum.STATUS):
            res["status"] = line.split(ProtocolEnum.STATUS)[1]

        elif line.startswith(ProtocolEnum.DATA):
            res["data"] = json.loads(line.split(ProtocolEnum.DATA)[1])

        elif line.startswith(ProtocolEnum.SESSION):
            res["session"] = line.split(ProtocolEnum.SESSION)[1]

        elif not line.startswith(ProtocolEnum.REQUEST_END):
            raise Exception('Can`t parse data')

    return res

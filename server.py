#!/usr/bin/python
import socket
import threading

# from protocl import parseProtocolFrame, buildProtocolFrame
from protocol import router


class ClientThread(threading.Thread):
    def __init__(self, connection):
        threading.Thread.__init__(self)
        self.connection = connection
        # ...

    def run(self):
        connection = self.connection

        try:
            while 1:
                data = router(connection)
                if data:
                    connection.sendall(data)
                pass
        except Exception, e:
            print str(e)
        finally:
            connection.close()
            pass


class Server:
    def __init__(self, ip, port):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_address = (ip, port)
        sock.bind(server_address)
        print ('Starting up TCP server on %s port %s' % server_address)

        sock.listen(5)
        self.sock = sock

    def run(self):
        while True:
            connection, client_address = self.sock.accept()
            try:
                c = ClientThread(connection)
                c.start()
                pass
            except Exception, e:
                print str(e)

            pass


if __name__ == '__main__':
    s = Server('127.0.0.1', 6666)
    s.run()
# while 1:
# 	connection, client_address = sock.accept()
# 	print ('Client %s connected ... ' % str(client_address))

# 	try:
# 		while  1:
# 			data = router(connection)
# 			if data:
# 				connection.sendall(data)
# finally:
#     connection.close()

import json
import os
import socket
import ssl
import urllib
import unicodedata
import urlparse

socket.setdefaulttimeout = 0.50
os.environ['no_proxy'] = '127.0.0.1,localhost'
CRLF = "\r\n\r\n"
PORT = 443


def buildUrl(text, langFrom, langTo):
    urlBase = "https://translate.yandex.net/api/v1.5/tr.json/translate?"
    params = urllib.urlencode({
        "key": "trnsl.1.1.20170615T131140Z.11230fc83c4dd9fa.57a7d422f0ac97108c032f67d7f03095b47599cd",
        "text": text.encode("utf-8"),
        "lang": "{0}-{1}".format(langFrom, langTo)
    })
    return urlBase + params


def GET(url):
    _url = urlparse.urlparse(url)
    path = _url.path
    if path == "":
        path = "/"
    HOST = _url.netloc  # The remote host

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ss = ssl.wrap_socket(s)
    ss.settimeout(10)
    ss.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    ss.connect((HOST, PORT))
    ss.sendall("GET {1} HTTP/1.1\r\nHost:{0}\r\n\r\n".format(HOST, url))
    response = (ss.recv(1000000))
    response = json.loads(response.split("\r\n").pop())
    ss.shutdown(1)
    ss.close()
    return response


# url = buildUrl("how are you bitch?", "en", "pl")
# print GET(url)
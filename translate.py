import unicodedata

import shttp
from RequestTypeEnum import RequestTypeEnum


def isTranslateFrame(frame):
    return frame["command"] == RequestTypeEnum.TRANSLATE


def translate(data):
    langFrom = unicodedata.normalize('NFKD', data["from"]).lower()
    langTo = unicodedata.normalize('NFKD', data["to"]).lower()
    # langFrom = data["from"]
    # langTo = data["to"]
    url = shttp.buildUrl(data["text"], langFrom, langTo)
    response = shttp.GET(url)
    if response["code"] == 200:
        res = ""
        if len(response["text"]) > 1:
            for translation in response["text"]:
                res += "- %s \n" % translation
        else:
            res = response["text"][0]

        return res
    else:
        raise Exception(response["message"])
